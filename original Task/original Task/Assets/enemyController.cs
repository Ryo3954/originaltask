﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyController : MonoBehaviour
{
    GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("player");
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(-0.1f, 0, 0);

        //画面外にでたオブジェクトを消す
        if (transform.position.x < -10.0f)
        {
            Destroy(gameObject);
        }

        //当たり判定(ベクトル）
        Vector2 p1 = transform.position; //敵の位置
        Vector2 p2 = player.transform.position;　//プレイヤーの位置
        Vector2 dir = p1 - p2;  //敵の位置ープレイヤーの位置
        //(スカラー）
        float d = dir.magnitude; //ベクトルの大きさ
        float r1 = 0.5f; //敵の半径
        float r2 = 1.0f;//プレイヤーの半径
        if (d < r1 + r2)
        {
            GameObject director = GameObject.Find("GameDirector");
            director.GetComponent<GameDirector>().DecreaseHP();
            Destroy(gameObject);

        }

      


    }
}
